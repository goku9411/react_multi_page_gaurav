import React from 'react';
import BookSearch from '../components/bookSearch';//from utils
import BooksList from './booksList';
// import utils from '../common/utils';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            data: []
        }
        this.handleSearchText = this.handleSearchText.bind(this);
        this.searchData = this.searchData.bind(this);
    }
    handleSearchText(event) {
        var value = event.target.value;
        this.setState({
            searchText: value
        })
    }
    searchData() {
        // window.location.href = window.location.hostname+window.location.port+'search/q='+ this.state.searchText;
		
       window.location.href = '/bookSearch/'+ this.state.searchText;
    }
    componentDidMount() {
        var main = this;
        fetch(`http://localhost:3000/booksData`).then(function(response){ //get path from utils
            return response.json();
        }).then(function(data){
            var currentURL = window.location.href;
            if(currentURL.indexOf('bookSearch') > 0) {
                let splitURL = currentURL.split('/');
                let searchString = splitURL[splitURL.length-1].toUpperCase();
                let dataToRender = [];
                let lengthOfItems = data.items.length;
                for(let i = 0; i < lengthOfItems ; i++) {
                    let description = data.items[i]['description'].toUpperCase();
                    let name = data.items[i]['name'].toUpperCase();
                    let id = data.items[i]['id'].toUpperCase();
                    if(description.indexOf(searchString)>=0 || name.indexOf(searchString)>=0 ||id.indexOf(searchString)>=0) {
                        dataToRender.push(data.items[i]);
                    }
                }
                main.setState({
                    data: dataToRender
                });
            }
            else {
                main.setState({
                    data: data.items
                });
            }
        })
        .catch(function(err){
            console.log('An Error Occured:', err);//error message from utils
        })
    }
    render() {
        return (            
            <div>                
                <BookSearch searchText={this.state.searchText} handleSearchText={this.handleSearchText} searchData={this.searchData} />
                <BooksList searchText={this.state.searchText} items={this.state.data}/>
            </div>
        )
    }
}
export default Home;