import React from 'react';

const BookSearch = (props) => {
    return (
        <form>
            <input type="text"  value={props.searchText} onChange={props.handleSearchText} />
            <input type="button" value="search" onClick={props.searchData}/>
        </form>

    )
}
export default BookSearch;