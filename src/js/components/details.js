import React from 'react';
const
    Details = (props) => {
    return (
            <li>
                <p>{props.itemData.id}</p>
                <p>{props.itemData.name}</p>
                <p>{props.itemData.description}</p>
                <img src={props.itemData.img} alt="boook"/>
                <button onClick={props.handleEdit}>Edit</button>
            </li>
    )
}
export default Details;