import React from 'react';
const EditDetail = (props) => {


    return (
            <form>
                <input type="number" value={props.itemData.id} onChange={props.handleDataChange.bind(this, [{name:'id'}])} />
                <input type="text" value={props.itemData.name} onChange={props.handleDataChange.bind(this, [{name:'name'}])} />
                <input type="text" value={props.itemData.description} onChange={props.handleDataChange.bind(this, [{name:'description'}])} />
                <input type="button" onClick={props.saveData} value="Edit" />
            </form>
    )
};
export default EditDetail;