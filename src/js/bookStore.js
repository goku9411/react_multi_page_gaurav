import React from 'react';
import Home from './containers/home';
import BookDetails from './containers/bookDetails';

import {BrowserRouter as Router, Route, Link, Switch, Redirect, browserHistory} from "react-router-dom"

class BookStore extends React.Component {
    componentDidMount() {
      /*  //!***** extra***!///
        var payload = {id:"sad"};

        var data = new FormData();
        data.append("json", JSON.stringify(payload));
        fetch(`/booksData`, {
            method: 'post',
            body:{id:"sad"}
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
         //   console.log('Created Gist:', data);
        });*/
    }
    render() {
        return (
            <Router history={browserHistory} >
                <Switch>
                    <Redirect exact from="/" to="/home"/>
                    <Route  path="/home" component={Home}/>
                    <Route  path="/bookSearch" component={Home}/>
                    <Route  path="/bookDetails" component={BookDetails}/>
                </Switch>
            </Router>
            /*
            <div>
                <Search searchText={this.state.searchText} handleSearchText={this.handleSearchText} searchData={this.searchData} />
                <ItemsList searchText={this.state.searchText} items={this.state.data}/>
            </div>*/
        )
    }
}
export default BookStore;